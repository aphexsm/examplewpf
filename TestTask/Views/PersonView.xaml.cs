﻿using TestTask.ViewModel;
using System.Windows;

namespace TestTask.Views
{
    /// <summary>
    /// Interaction logic for PersonView.xaml
    /// </summary>
    public partial class PersonView : Window
    {
        public PersonView()
        {
            InitializeComponent();

            var viewModel = new PersonViewModel();

            DataContext = viewModel;
        }
    }
}
