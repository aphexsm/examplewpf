﻿using CsvHelper;
using TestTask.Helpers;
using TestTask.Model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace TestTask.ViewModel
{
    class PersonViewModel : BaseViewModel
    {
        public ObservableCollection<PersonalData> Persons { get; set; }

        public string Notification { get; set; }

        public ICommand ExportToCsvCommand { get; set; }
        public ICommand GetDataFromDatabaseCommand { get; set; }
        public ICommand AddPersonToDatabaseCommand { get; set; }

        public PersonalData CurrentPerson { get; set; }

        public PersonViewModel()
        {
            Persons = new ObservableCollection<PersonalData>();
            CurrentPerson = new PersonalData();

            try
            {
                DataManager.ConnectToDatabase();
            }
            catch(Exception)
            {
                MessageBox.Show("Ошибка при подключении к базе данных");
            }

            ExportToCsvCommand = new RelayCommand(ExportToCsvFile);
            GetDataFromDatabaseCommand = new RelayCommand(GetDataFromDatabase);
            AddPersonToDatabaseCommand = new RelayCommand(AddPersonToDatabase);
        }

        private void AddPersonToDatabase(object parameter)
        {
            try
            {
                DataManager.UpdateDatabase(CurrentPerson);

                SetNotification("Добавлена новая запись в базу данных");
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка при добавлении в базу данных");
            }
        }

        private void GetDataFromDatabase(object parameter)
        {
            try
            {
                DataManager.GetAllEntriesFromDatabase();

                Persons = new ObservableCollection<PersonalData>(DataManager.Persons);

                OnPropertyChanged("Persons");
                SetNotification("Прочитаны все записи из базы данных");
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка при чтении из базы данных");
            }
        }

        private void ExportToCsvFile(object parameter)
        {
            var persons = GetSortedPersonalData();

            if (persons == null)
            {
                return;
            }

            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                Filter = "Файлы (*.csv)|*.csv"
            };

            if (saveFileDialog.ShowDialog() == true)
            {
                StringWriter csvFile = new StringWriter();

                CsvWriter csv = new CsvWriter(csvFile);

                foreach (var person in persons)
                {
                    CsvPersonalData record = new CsvPersonalData
                    {
                        Lastname = person.Lastname,
                        Name = person.Name,
                        Patronymic = person.Patronymic,
                        Birthday = person.Birthday.ToShortDateString(),
                        PassportSeries = person.Passport.Substring(0, 4),
                        Passport = person.Passport.Substring(0, 4) + "-" + person.Passport.Substring(4)
                    };

                    csv.WriteRecord(record);
                    csv.NextRecord();
                }
                
                File.WriteAllText(saveFileDialog.FileName, csvFile.ToString());
                SetNotification("Данные экспортированы в CSV файл");
            }
        }

        private List<PersonalData> GetSortedPersonalData()
        {
            try
            {
                DataManager.GetAllEntriesFromDatabase();

                var sorted = from person in DataManager.Persons
                             orderby person.Lastname, person.Name, person.Patronymic, person.Birthday
                             select person;

                return sorted.ToList();
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка при чтении из базы данных");
                return null;
            }
        }

        private void SetNotification(string text)
        {
            Notification = text;
            OnPropertyChanged("Notification");
        }
    }
}
