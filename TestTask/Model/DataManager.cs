﻿using System.Collections.Generic;

namespace TestTask.Model
{
    class DataManager
    {
        public static List<PersonalData> Persons { get; private set; }

        private static Database db;

        public static void ConnectToDatabase()
        {
            Persons = new List<PersonalData>();

            db = new Database("TestTask.sqlite");

            db.ConnectToDatabase();
        }

        public static void UpdateDatabase(PersonalData person)
        {
            db.UpdateDatabase(person);
        }

        public static void GetAllEntriesFromDatabase()
        {
            Persons = db.ReadAllDatabase();
        }
    }
}
