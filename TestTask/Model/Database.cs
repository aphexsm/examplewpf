﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;

namespace TestTask.Model
{
    class Database
    {
        private SQLiteConnection dbConnection;
        private readonly string dbName;

        public Database(string _dbName)
        {
            dbName = _dbName;

            if (!File.Exists(dbName))
            {
                CreateDataBase();
            }
        }

        private void CreateDataBase()
        {
            SQLiteConnection.CreateFile(dbName);

            ConnectToDatabase();

            CreateEmptyTable();
        }

        public void ConnectToDatabase()
        {
            dbConnection = new SQLiteConnection("Data Source=" + dbName + ";Version=3; FailIfMissing=True");
            dbConnection.Open();
        }

        private void CreateEmptyTable()
        {
            string sql = "create table if not exists PersonalData (lastname TEXT, name TEXT, patronymic TEXT, birthday INTEGER, passport TEXT);";

            SQLiteCommand command = new SQLiteCommand(sql, dbConnection);

            command.ExecuteNonQuery();
        }

        public void UpdateDatabase(PersonalData person)
        {
            string sql = "insert into PersonalData (lastname, name, patronymic, birthday, passport) " +
                         "values (@lastname, @name, @patronymic, @birthday, @passport);";

            SQLiteCommand command = new SQLiteCommand(sql, dbConnection);

            command.Parameters.AddWithValue("@lastname", person.Lastname);
            command.Parameters.AddWithValue("@name", person.Name);
            command.Parameters.AddWithValue("@patronymic", person.Patronymic);
            command.Parameters.AddWithValue("@birthday", ((DateTimeOffset)person.Birthday).ToUnixTimeSeconds());
            command.Parameters.AddWithValue("@passport", person.Passport);

            command.ExecuteNonQuery();
        }

        public List<PersonalData> ReadAllDatabase()
        {
            string sql = "select * from PersonalData;";

            SQLiteCommand command = new SQLiteCommand(sql, dbConnection);

            SQLiteDataReader reader = command.ExecuteReader();

            List<PersonalData> persons = new List<PersonalData>();

            while(reader.Read())
            {
                PersonalData person = new PersonalData
                {
                    Lastname = reader["lastname"].ToString(),
                    Name = reader["name"].ToString(),
                    Patronymic = reader["patronymic"].ToString(),
                    Passport = reader["passport"].ToString()
                };

                long.TryParse(reader["birthday"].ToString(), out long birthday);

                person.Birthday = DateTimeOffset.FromUnixTimeSeconds(birthday).ToLocalTime().DateTime;

                persons.Add(person);
            }

            return persons;
        }
    }
}
