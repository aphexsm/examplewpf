﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace TestTask.Model
{
    class PersonalData : IDataErrorInfo
    {
        [DisplayName("Фамилия")]
        public string Lastname { get; set; }

        [DisplayName("Имя")]
        public string Name { get; set; }

        [DisplayName("Отчество")]
        public string Patronymic { get; set; }

        [DisplayName("Дата рождения")]
        public DateTime Birthday { get; set; }

        [DisplayName("Паспорт")]
        public string Passport { get; set; }

        public PersonalData()
        {
            Lastname = "";
            Name = "";
            Patronymic = "";
            Passport = "";
            Birthday = DateTime.Now;
        }

        #region IDataErrorInfo fields

        public string Error => null;

        public string this[string columnName]
        {
            get
            {
                string result = string.Empty;

                if (columnName == "Lastname")
                {
                    if (!ValidateCyrillicLetters(Lastname))
                    {
                        return "Wrong format";
                    }
                }

                if (columnName == "Name")
                {
                    if (!ValidateCyrillicLetters(Name))
                    {
                        return "Wrong format";
                    }
                }

                if(columnName == "Patronymic")
                {
                    if (!ValidateCyrillicLetters(Patronymic, false))
                    {
                        return "Wrong format";
                    }
                }

                if (columnName == "Passport")
                {
                    if (!ValidatePassport(Passport))
                    {
                        return "Wrong format";
                    }
                }

                return result;
            }
        }

        bool ValidateCyrillicLetters(string field, bool checkForEmptyString = true)
        {
            Regex cyrillicRegex = new Regex("[^\\p{IsCyrillic}\\s-]");

            if ((checkForEmptyString && field == "") || cyrillicRegex.IsMatch(field))
            {
                return false;
            }

            return true;
        }

        bool ValidatePassport(string field)
        {
            Regex numbersRegex = new Regex("^[0-9]+$");

            if (field.Length != 10 || !numbersRegex.IsMatch(field))
            {
                return false;
            }

            return true;
        }
        #endregion
    }

    class CsvPersonalData
    {
        public string Lastname { get; set; }
        public string Name { get; set; }
        public string Patronymic { get; set; }

        public string Birthday { get; set; }

        public string PassportSeries { get; set; }
        public string Passport { get; set; }
    }
}
